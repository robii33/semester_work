from main.viewsets import TaskViewset, UserViewset, ChatViewset, SetOfTasksViewset, AnswerViewset
from rest_framework import routers

router = routers.DefaultRouter()

router.register('task', TaskViewset)
router.register('user', UserViewset)
router.register('chat', ChatViewset)
router.register('set-of-tasks', SetOfTasksViewset)
router.register('answer', AnswerViewset)
