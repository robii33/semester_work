# Generated by Django 4.1 on 2023-01-22 10:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_rename_пользователь_answer_user_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='date',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата'),
        ),
        migrations.AddField(
            model_name='task',
            name='difficulty',
            field=models.IntegerField(blank=True, null=True, verbose_name='Сложность'),
        ),
    ]
