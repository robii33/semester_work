# Generated by Django 4.0.6 on 2022-07-12 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_superuser_remove_user_is_active_remove_user_is_staff_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='setoftasks',
            options={'verbose_name': 'Группа заданий', 'verbose_name_plural': 'Группы заданий'},
        ),
        migrations.AlterField(
            model_name='setoftasks',
            name='task',
            field=models.ManyToManyField(related_name='set_of_tasks', to='main.task', verbose_name='Задание'),
        ),
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Имя')),
                ('chat_history', models.TextField(verbose_name='История чата')),
                ('user', models.ManyToManyField(related_name='user_chat', to='main.user', verbose_name='Пользователь')),
                ('Админ', models.ManyToManyField(related_name='superuser_chat', to='main.superuser')),
            ],
        ),
    ]
