# Generated by Django 4.0.6 on 2022-07-12 20:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SuperUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=255, unique=True, verbose_name='email адрес')),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=255, verbose_name='Фамилия')),
                ('password_text', models.CharField(max_length=50, verbose_name='Пароль')),
                ('photo', models.ImageField(blank=True, null=True, upload_to='users/photos', verbose_name='Фото')),
                ('bio', models.TextField(blank=True, null=True, verbose_name='О себе')),
            ],
            options={
                'verbose_name': 'Админ',
                'verbose_name_plural': 'Админы',
            },
        ),
        migrations.RemoveField(
            model_name='user',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='user',
            name='is_staff',
        ),
        migrations.RemoveField(
            model_name='user',
            name='is_superuser',
        ),
        migrations.CreateModel(
            name='SetOfTasks',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Имя')),
                ('description', models.CharField(max_length=255, verbose_name='Описание')),
                ('task', models.ManyToManyField(related_name='set_of_tasks', to='main.task', verbose_name='Группа заданий')),
            ],
            options={
                'verbose_name': 'Задания',
                'verbose_name_plural': 'Задания',
            },
        ),
    ]
