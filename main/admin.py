from django.contrib import admin

from simple_history.admin import SimpleHistoryAdmin

from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field

from .models import Answer, Task, User, Role, SetOfTasks, Chat


admin.site.site_title  = 'Админ панель сервиса собеседований'
admin.site.site_header  = 'Админ панель сервиса собеседований'

class TaskResource(resources.ModelResource):
    creator = Field()
    
    class Meta:
        model = Task
        import_id_fields = ('id',)
        exclude = ('history', 'date2',)

        def dehydrate_creator(self, task):
            creator = User.objects.get(pk=task.creator)

            return creator.email


class UserResource(resources.ModelResource):
    class Meta:
        model = User
        exclude = ('password_text', 'history',)


class TaskAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('title', 'task', 'creator', 'difficulty', 'date2')
    search_fields = ('title', 'task')
    history_list_display = ['title', 'task', 'creator', 'difficulty', 'date2']
    resource_classes = [TaskResource]


class UserAdmin(SimpleHistoryAdmin, ImportExportModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'role')
    search_fields = ('email', 'first_name', 'last_name')
    history_list_display = ['email', 'first_name', 'last_name', 'role']
    resource_classes = [UserResource]


class RoleAdmin(admin.ModelAdmin):
    list_display = ('role',)
    search_fields = ('role',)
    


class ChatAdmin(SimpleHistoryAdmin):
    list_display = ('title',)
    search_fields = ('title', 'user')
    history_list_display = ['title', 'user']


class SetOfTasksAdmin(SimpleHistoryAdmin):
    list_display = ('title', 'description')
    search_fields = ('tile', 'description')
    history_list_display = ['title', 'description', 'task']
    

class AnswerAdmin(SimpleHistoryAdmin):
    list_display = ('answer_text', 'date', 'user', 'task')
    search_fields = ('answer_text',)
    history_list_display = ['answer_text', 'date', 'user', 'task']
    

admin.site.register(Task, TaskAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(SetOfTasks, SetOfTasksAdmin)
admin.site.register(Chat, ChatAdmin)