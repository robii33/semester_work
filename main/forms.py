from django.forms import ModelForm, TextInput, Textarea

from .models import Answer, Task, User


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'task']
        widgets = {
            'title': TextInput(attrs={
                'class': 'main__input form-control',
                'placeholder': 'Введите название',
            }),
            'task': Textarea(attrs={
                'class': 'main__input form-control',
                'placeholder': 'Введите описание',
            }),
        }


class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ['answer_text', 'task']
        widgets = {
            'answer_text': Textarea(attrs={
                'class': "main__input task-textarea form-control mw-100",
                'name':  'answer',
                'id': 'answer',
                'rows': '25',
                'required': True,
            }),
            'task': TextInput(attrs={
                # 'disabled': True,
                'class': 'main__input task-id',
                'name': 'task-id',
            })
        }


class RegisterForm(ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'password_text']
        widgets = {
            'email': TextInput(attrs={
                'class': 'main__input form-control register-input',
                'placeholder': 'Введите название',
                'required': True,
                
            }),
            'first_name': TextInput(attrs={
                'class': 'main__input form-control register-input',
                'placeholder': 'Введите название',
                'required': True,
            }),
            'last_name': TextInput(attrs={
                'class': 'main__input form-control register-input',
                'placeholder': 'Введите название',
                'required': True,

            }),
            'password_text': TextInput(attrs={
                'class': 'main__input form-control register-input',
                'placeholder': 'Введите название',
                'required': True,
                'type': 'password',

            })
        }
            