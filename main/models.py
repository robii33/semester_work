from statistics import mode
from tabnanny import verbose
from django.db import models
import datetime
from simple_history.models import HistoricalRecords


class Role(models.Model):
    role = models.CharField('Роль', max_length=50)
    

    def __str__(self):
        return self.role

    class Meta:
        verbose_name = 'Роль'
        verbose_name_plural = 'Роли'
        


class User(models.Model):
    email = models.EmailField(verbose_name='email адрес', max_length=255, unique=True)
    first_name = models.CharField(verbose_name='Имя', max_length=255)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255)
    password_text = models.CharField(verbose_name='Пароль', max_length=50)
    photo = models.ImageField(verbose_name='Фото', upload_to='users/photos', blank=True, null=True)
    bio = models.TextField(verbose_name='О себе', blank=True, null=True)
    role = models.ForeignKey(to=Role, related_name="role_role", on_delete=models.CASCADE, blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Task(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField('Название', max_length=50)
    task = models.TextField('Описание')
    creator = models.ForeignKey(to=User, verbose_name="Создатель", name="creator", on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField(verbose_name="Дата", auto_now_add=True, blank=True, null=True)
    date2 = models.DateField(verbose_name="Дата создания", auto_now_add=True, blank=True, null=True)
    difficulty = models.IntegerField(verbose_name="Сложность", blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self): 
        return self.title

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'


class Chat(models.Model):
    title = models.CharField(verbose_name='Имя', max_length=255)
    user = models.ManyToManyField(to=User, verbose_name='Пользователь', related_name='user_chat')
    chat_history = models.TextField(verbose_name='История чата')
    history = HistoricalRecords()

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Чат'
        verbose_name_plural = 'Чаты'


class SetOfTasks(models.Model):
    title = models.CharField(verbose_name='Имя', max_length=255)
    description = models.CharField(verbose_name='Описание', max_length=255)
    task = models.ManyToManyField(verbose_name="Задание", to=Task, related_name='set_of_tasks')
    history = HistoricalRecords()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Группа заданий'
        verbose_name_plural = 'Группы заданий'


class Answer(models.Model):
    answer_text = models.TextField('Ответ')
    date = models.DateField('Дата', auto_now_add=True, blank = True, null=True)
    user = models.ForeignKey(to=User, name="user", related_name="user_task", on_delete=models.CASCADE, blank=True, null=True)
    task = models.ForeignKey(to=Task, name="task", related_name="task_answer", on_delete=models.CASCADE, blank=True, null=True)

    history = HistoricalRecords()

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
