from datetime import date
from django.core.exceptions import ValidationError

from rest_framework import viewsets

from django.db.models import Q

from rest_framework.decorators import action
from rest_framework.filters import SearchFilter

from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from . import serializers
from . import models


class TaskPagination(PageNumberPagination):
    page_size = 3
    page_size_query_param = 'page_size'
    max_page_size = 10
    

# api/task
class TaskViewset(viewsets.ModelViewSet):
    queryset = models.Task.objects.all()
    serializer_class = serializers.TaskSerializer
    pagination_class = TaskPagination
    filter_backends = [SearchFilter]
    search_fields = ['id', 'title']

    # ../latest-difficult-tasks/
    @action(methods=["get"], detail=False, url_path="latest-difficult-tasks")
    def latest_difficul_tasks(self, request):
        try:
            date_param = request.query_params.get('date')
            query = models.Task.objects.filter(
                Q(date2=str(date_param if date_param is not None else date.today())),
                Q(difficulty=10) | Q(difficulty=9) | Q(difficulty=8)
            ).values_list('id', flat=True)

        except (ValueError, ValidationError) as error:
            if isinstance(error, ValidationError):
                return Response({'response': 'Incorrect date query.'})

            return Response({'response': 'No tasks found.'})
            
        return Response({'response': [i for i in query]})


# api/user
class UserViewset(viewsets.ModelViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer

    # ../count-users/
    @action(methods=["get"], detail=False, url_path='count-users')
    def count_users(self, request):
        return Response({'quantity': self.queryset.count() })

    # ../<pk>/all-user-tasks/
    @action(methods=["get"], detail=True, url_path='all-user-tasks')
    def all_user_tasks(self, request, pk=None):

        if pk is not None:
            query = models.Task.objects.filter(creator=pk).values_list('id', flat=True)
            return Response({'tasks': [int(i) for i in query]})


class AnswerViewset(viewsets.ModelViewSet):
    queryset = models.Answer.objects.all()
    serializer_class = serializers.AnswerSerializer


class SetOfTasksViewset(viewsets.ModelViewSet):
    queryset = models.SetOfTasks.objects.all()
    serializer_class = serializers.SetOfTasksSerializer


class ChatViewset(viewsets.ModelViewSet):
    queryset = models.Chat.objects.all()
    serializer_class = serializers.ChatSerializer