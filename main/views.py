from django.shortcuts import render, redirect
from .models import Task
from .forms import AnswerForm, RegisterForm, TaskForm


def index(request):
    tasks = Task.objects.all()
    return render(request, 'main/index.html', {'ids': tasks.values_list('id', flat=True), 'title': 'Главная страница', 'tasks': tasks})


def create(request):
    error = ''
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('home')
        else:
            error = 'Неверная форма'

    form = TaskForm()
    context = {
        'form': form,
        'error': error,
    }
    return render(request, 'main/create.html', context)

def task(request):
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            print(request.POST)

    task = Task.objects.get(pk=request.GET['id'])
    
    form = AnswerForm(initial={'task': task.id})
    context = {
        'form': form,
        'task': task
    }
    return render(request, 'main/task.html', context)

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            print(request.POST)

    form = RegisterForm()
    context = {
        'form': form
    }
    return render(request, 'main/register.html', context)

def update(request, pk):
    error = ''

    task = Task.objects.get(id=pk)
    form = TaskForm(instance=task)

    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)

        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            error = "Invalid form"
    
    
    context = {
        'form': form,
        'error': error,
    }
    return render(request, 'main/update.html', context)

def delete(request, pk):

    task = Task.objects.get(id=pk)
    task.delete()

    return redirect('home')