from rest_framework.serializers import (
    EmailField,
    ModelSerializer,
    ValidationError
)

from .models import Task, User, Answer, SetOfTasks, Chat 


class TaskSerializer(ModelSerializer):
    class Meta:
        model = Task 
        fields = '__all__'
        

class UserSerializer(ModelSerializer):
    email2 = EmailField(label="Confirm email", write_only=True)
    
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'email2',
            'password_text'
        ]
        extra_kwargs = {"password_text":
            {"write_only": True}
        }

    def validate_email2(self, value):
        data = self.get_initial()
        email1 = data.get("email")
        email2 = value

        if email1 != email2:
            raise ValidationError("Emails do not match.")
            
        return value

    def create(self, data):
        first_name = data['first_name']
        last_name = data['last_name']
        email = data['email']
        password = data['password_text']

        user_object = User(
            first_name = first_name,
            last_name = last_name,
            email = email,
            password_text = password,
        )
        user_object.save()
        
        return data


class AnswerSerializer(ModelSerializer):
    class Meta:
        model = Answer 
        fields = ('answer_text', 'date', 'user')


class SetOfTasksSerializer(ModelSerializer):
    class Meta:
        model = SetOfTasks
        fields = '__all__'


class ChatSerializer(ModelSerializer):
    class Meta:
        model = Chat
        fields = '__all__'